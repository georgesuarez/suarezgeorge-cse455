package com.example.georgesuarez.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String userInput01;

    private static final String url = "https://api.fixer.io/latest?base=USD";

    String json = "";
    String line = "";
    String rate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText01 = findViewById(R.id.EditText01);
        bnt01 = findViewById(R.id.bnt01);
        textView01 = findViewById(R.id.Yen);


        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertCurrency) {
                System.out.println("\nTESTING 1 ... Before AsynchExecution\n");

                BackgroundTask object = new BackgroundTask();

                object.execute();

                System.out.println("\nTESTING 2 ... After AsynchExecution\n");
            }
        });
    }

    private class BackgroundTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            try {
                URL webUrl = new URL(MainActivity.this.url);

                HttpURLConnection httpURLConnection = (HttpURLConnection) webUrl.openConnection();

                httpURLConnection.setRequestMethod("GET");

                System.out.println("\nTESTING ... BEFORE connection method to URL\n");

                httpURLConnection.connect();

                // Create object from the class InputStream and initialize object with
                InputStream inputStream = httpURLConnection.getInputStream();

                // Create object named bufferedReader from the BufferedReader class and initialize the object
                // with new BufferedStream(new InputStreamReader(inputStream))
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                System.out.println("CONNECTION SUCCESSFUL\n");

                while (line != null) {
                    line = bufferedReader.readLine();
                    json += line;
                }
                System.out.println("\nTHE JSON: " + json);

                JSONObject obj = new JSONObject(json);

                JSONObject objRate = obj.getJSONObject("rates");

                rate = objRate.get("JPY").toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                Log.d("MYAPP","unexpected JSON exception", e);
                System.exit(1);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            System.out.println("What is rate " + rate + "\n");

            double value = Double.parseDouble(rate);

            System.out.println("\nTesting JSON String Exchange Rate INSIDE AsyncTask: " + value);

            userInput01 = editText01.getText().toString();
            if (userInput01.equals("")) {
                textView01.setText("This field cannot be blank!");
            } else {
                double dInputs = Double.parseDouble(userInput01);
                double toYen = dInputs * 112.57;

                textView01.setText("$" + userInput01 + " = " + "¥" + String.format("%.2f", toYen));
                editText01.setText("");
            }
        }
    }
}
