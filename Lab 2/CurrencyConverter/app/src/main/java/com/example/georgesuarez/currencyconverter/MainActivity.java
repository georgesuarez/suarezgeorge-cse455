package com.example.georgesuarez.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText01 = findViewById(R.id.EditText01);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);

        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {
                usd = editText01.getText().toString();
                if (usd.equals("")) {
                    textView01.setText("This field cannot be blank!");
                } else {
                    double dInputs = Double.parseDouble(usd);
                    double result = dInputs * 112.57;

                    textView01.setText("$" + usd + " = " + "¥" + String.format("%.2f", result));

                    editText01.setText("");
                }
            }
        });
    }
}
